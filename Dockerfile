FROM ubuntu

RUN apt-get update && \
    apt-get install -y software-properties-common apt-transport-https ca-certificates curl gnupg2 unzip && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -  && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
    apt-get update  && \
    apt-get install -y docker-ce && \
    apt-get install -y nodejs && \
    apt-get install -y npm  && \
    curl https://sdk.cloud.google.com | bash && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl   && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    curl -O -L --remote-name https://releases.hashicorp.com/terraform/0.12.20/terraform_0.12.20_linux_amd64.zip && \
    unzip terraform_0.12.20_linux_amd64.zip && \
    mv ./terraform /usr/local/bin/


ENV PATH=${PATH}:/root/google-cloud-sdk/bin

EXPOSE 8080