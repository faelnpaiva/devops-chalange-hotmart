resource "google_compute_network" "container_network" {
  name                    = "${var.network_name}"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "container_subnetwork" {
  name          = "${var.subnet_name}"
  description   = "auto-created subnetwork for cluster "
  ip_cidr_range = "${var.cidr_range}" 
  region        = "${var.region}"
  network       = "${google_compute_network.container_network.self_link}"
}
