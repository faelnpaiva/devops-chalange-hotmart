output "network" {
  value = "${google_compute_network.container_network.name}"
}

output "subnetwork" {
  value = "${google_compute_subnetwork.container_subnetwork.name}"
}
