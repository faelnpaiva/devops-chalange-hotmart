# devops-chalange-003

====TERRAFORM====
Os modulos de terraform para provisionar o cluster são:
- network ( VPC/SUBNET/FIREWALL )
- cluster
Fiz uma demonstração de um modulo conversando com o outro ( network com cluster ) . Fiz um backend, mas não precisei utilizar. 

====APLICAÇÃO=====
Chamadas pelo CI/CD da pasta application ( Kubernetes usado diretamente )
Assim que tudo estiver provisionado por favor clicar no ip gerado no loadbalancer (http)
loadbalancer ( foi usado kubernetes diretamente )


====CI/CD=====
Foram criados 5 estágios visiveis para o branch master e  3 para os demais branches ( lint , build , deploy)
- terraform plan
- terraform apply - provisiona o cluster 
- lint - leve teste executado no source code
- build - conteineriza o source code e sobe no dockerhub
- deploy - de acordo com o branch que recebeu uma submissão de código , o cluster é provisionado ( se não houver mudança, não faz update no cluster), faz o deploy do nginx-ingress controller e do loadbalancer. 
- O ingress para a aplicação é deployado juntamente com a própria aplicação e serviço . Deploy production ( branch master ) manual ( aperta o play no ci/cd .
Script popupating.sh esta pegando as variaveis de ambiente do gitlabCI e gerando um tfvars para sobrescrever o variables.tf dos arquivos externos aos modulos ( usado para "transmitir" os valores/outputs para os modulos )
- terraform destroy- destroi o cluster ( step manual )

=====Docker=====
Dockerfile principal usado para gerar a imagem a ser usada dentro do gitlabCI.
Dockerfile dentro do source code usado para conteinerizar a imagem .
Kaniko usado para rodar docker dentro de docker (https://docs.gitlab.com/ee/ci/docker/using_kaniko.html )






====OBSERVAÇÕES GERAIS====
Separei os deploys por namespace ( production / staging) , embora seja melhor pratica separá-los por cluster . 
Não tive muito tempo para caprichar , devo ter gasto por volta de ~10h na semana para fazer. 
Utilizei kubernetes diretamente sem utilizar o helm chart pois o tiller estava dando muito trabalho para configurar com o terraform .
Documentação esta verbosa por motivo de falta de tempo, gostaria de ter colocado um fluxograma .
Não implementei um rollback para os deploys ( poderia ser mais um estagio do CI/CD por exemplo).
O terraform poderia ter sido mais abstraido com relação as variaveis e poderia ter sido separado por Workspaces .
Faltou resolução de DNS com o terraform ( https://www.terraform.io/docs/providers/dns/index.html ). Não consegui um dominio valido também . Os steps manuais para configurar o DNS são: criar um dominio valido na sessão de DNS, criar um wildcard (recordset) e apontar o IP do loadbalancer pra ele .





