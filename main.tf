terraform {
  required_version = "0.12.20"
}

provider "google" {
  credentials = "${file("/tmp/auth.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
  zone        = "${var.zone}"
}
module "cluster" {
  source = "./cluster"
  project="${var.project}"
  region ="${var.region}"
  zone = "${var.zone}"
  cluster = "${var.cluster_name}"
  node_name ="${var.node_name}"
  network = "${module.network.network}"
  subnetwork= "${module.network.subnetwork}"  
}
module "network" {
  source = "./network"
  network_name="${var.network}"
  subnet_name="${var.subnetwork}"
  cidr_range="${var.cidr_range}"
  region="${var.region}"
  firewall="${var.firewall}"
}
