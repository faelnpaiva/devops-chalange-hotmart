#!/bin/bash



echo -e "project=\"${PROJECT}\"\
\nregion=\"${REGION}\"\
\nzone=\"${ZONE}\"\
\nnode_name=\"${NODE_NAME}\"\
\ncluster_name=\"${CLUSTER}\"\
\nnetwork=\"${VPC_NAME}\"\
\nsubnetwork=\"${SUBNET_NAME}\"\
\ncidr_range=\"${CIDR_RANGE}\"\
\nfirewall=\"${FIREWALL}\"\
" > terraform.tfvars

