terraform {
  backend "gcs" {
    bucket  = "tf-state-hotmart"
    prefix  = "terraform/state/production"
    credentials  = "/tmp/auth.json"
    
  }
}
